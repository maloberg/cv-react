import Header from "./header/Header";
import Slides from "./Slides";
import React from "react";

function App() {
  return (
    <div className="App">
        <Header/>
        <div className="slider">
            <Slides/>
        </div>

    </div>
  );
}

export default App;
