import React, {useReducer} from 'react';
import Competences from "./body/Competences";
import Profil from "./body/Profil";
import Education from "./body/Education";
import './slide.scss'
import WorkExperiences from "./body/WorkExperiences";
import Information from "./body/Information";
import me from'./img/malorogner.jpg'
const components = [
    {
        title:'Information',
        components: <Information/>,
        image:'https://img.freepik.com/photos-gratuite/chalkboard-enveloppes-bleues-dessinees-main_1205-996.jpg?size=338&ext=jpg'
    },
    {
        title:'Formation',
        components: <Education/>,
        image:'https://img.freepik.com/photos-gratuite/planche-mortier-diplome_53876-47088.jpg?size=338&ext=jpg'
    },
    {
        title:'Experiences Professionnel',
        components: <WorkExperiences/>,
        image:'https://image.freepik.com/photos-gratuite/concept-equipe-travail-equipe-travail_53876-23476.jpg'
    },
    {
        title:'Profil',
        components: <Profil/>,
        image: me
    },
    {
        title:'competences',
        components: <Competences/>,
        image:'https://image.freepik.com/photos-gratuite/vue-dessus-competences-note-ecrite-stylo-bloc-notes-fond-blanc-bureau-emploi-cahier-ecole-salaire-college-business-color_179666-18277.jpg'
    },

]
const initialState = {
    slideIndex: 4
};

const slidesReducer = (state,event) => {
    if (event.type === "NEXT") {
        return {
            slideIndex: (state.slideIndex + 1) % components.length
        };
    }
    if (event.type === "PREV") {
        return {
            slideIndex: state.slideIndex === 0 ? components.length - 1 : state.slideIndex - 1
        };
    }
};

function Slide({ slide, offset }) {
    const active = offset === 0 ? true : null;
    return (
        <div
            className="slide"
            data-active={active}
            style={{
                "--offset": offset,
                "--dir": offset === 0 ? 0 : offset > 0 ? 1 : -1
            }}
        >
            <div
                className="slideBackground"
                style={{
                    backgroundImage: `url('${slide.image}')`
                }}
            />
            <div
                className="slideContent"
                style={{
                    backgroundImage: `url('${slide.image}')`
                }}
            >
                <div className="slideContentInner">
                    <h2 className="slideTitle">{slide.title}</h2>
                    {slide.components}
                </div>
            </div>
        </div>
    );
}


function Slides(props) {
    const [state, dispatch] = useReducer(slidesReducer, initialState);

    return (
        <div className="slides">
            <button onClick={() => dispatch({ type: "NEXT" })}>‹</button>

            {[...components, ...components, ...components].map((component, i) => {
                let offset = components.length + (state.slideIndex - i);
                return <Slide slide={component} offset={offset} key={i} />;

            })}
            <button onClick={() => dispatch({ type: "PREV" })}>›</button>
        </div>
    );
}

export default Slides;
