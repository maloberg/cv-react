import React, {useState} from 'react';


function ContacList()
{
    return  <ul>
        <li>06 59 45 04 29</li>
        <li><a href="mailto:malo.bergade@gmail.com">malo.bergade@gmail.com</a></li>
        <li><a href="https://gitlab.com/maloberg">Mon dépot git</a></li>
        <li>Permis B et voiture</li>
    </ul>


}


function Contact(props) {
    const [show, setShow] = useState(false)
    function onContactClick()
    {
        show ? setShow(false): setShow(true);
    }

    return (
        <div className="contact">
            <button className="btn-contact" onClick={onContactClick}>Informations</button>
            {show && <ContacList/>}
        </div>
    );
}

export default Contact;