import React from 'react';
import Contact from "./Contact";
import Picture from "./Picture";
import './header.scss';

function Header(props) {
    return (
        <header>
            <div className="title"><h1 >Stagiaire développeur front et back-end</h1></div>
            <div><h3>Malo BERGADE</h3></div>

            <Picture/>
        </header>
    );
}

export default Header;