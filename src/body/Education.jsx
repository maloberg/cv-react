import React from 'react';

function EducationList({date, children, location, description})
{
    return <li><strong>{description}
        </strong>
        <p>{date}/<em> {location}</em></p>
        <p>{children}</p></li>
}

function Education(props) {
    return (
        <section id="education" className="education">
            <ol>
                <EducationList
                    date="En cours"
                    description="Titre de developpeur web et web mobile"
                    location="66270 Le Soler" />

                <EducationList
                    date="2020"
                    description="Piscine de l'Ecole 42"
                    location="75017 Paris">
                    Ce fut une expérience tres enrichissante qui ma appris à
                    être beaucoup plus autonome.
                </EducationList>

                <EducationList
                    date="2018-2019"
                    location="66000 Perpignan"
                    description="Première année de DUT Gestion des administrations et entreprises"
                />

                <EducationList
                    date="2018"
                    location="66000 Perpignan"
                    description="Baccalauréat scientifique"
                />
            </ol>
        </section>
    );
}

export default Education;