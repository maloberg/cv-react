import React from 'react';

function Profil(props) {
    return (
        <section className="profil">
            <div>
                <p>
                    Stagiaire développeur front-end et back-end, avec un enthousiasme autour du développement très
                    important.
                </p>
                <br/>
                <p>
                    Je suis intéréssé par le métier de développeur full-stack, par l'autonomie obligatoire dans une
                    telle
                    spécialisation et l'apprentissage permanent. J'ai la volonté nécéssaire pour améliorer mes
                    connaissances, mes compétences
                    ainsi que la qualité de mon travail. De plus je suis extrêmement intéréssé par votre entreprise car
                    la création de bot apporte un environnement différent du domaine du web commun.
                </p>
            </div>
        </section>

    );
}

export default Profil;