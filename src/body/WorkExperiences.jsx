import React from 'react';

function WorkExperiencesList({title, children})
{
    return <li>
        <strong>{title}</strong>
        <ul>
            <li>{children}</li>
        </ul>
    </li>
}

function WorkExperiences(props) {
    return (

        <section id="work-experiences" className="work-experiences">
      
            <ol>
                <WorkExperiencesList title="2020/ Bénévole Resto du coeur">
                    Lors de cette expérience dans le bénévolat j'ai été en charge de la mise a jour quotidienne
                    des données informatique du centre.
                </WorkExperiencesList><br/>
                <WorkExperiencesList title="2019/ Manutentionnaire Ille roussillon">
                    En tant que gestionnaire de fin de chaîne, il fallait comptabiliser les palettes,
                    s'organiser pour indiquer à l'équipe en question les commandes nécessaires et dans quel
                    ordre les effectuer.
                </WorkExperiencesList>
            </ol>
        </section>
    );
}

export default WorkExperiences;