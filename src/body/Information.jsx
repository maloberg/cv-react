import React from 'react';

function Information(props) {
    return (
        <div>
            <ul>
                <li>06 59 45 04 29</li>
                <li><a href="mailto:malo.bergade@gmail.com">malo.bergade@gmail.com</a></li>
                <li>Detenteur du permis B et d'une voiture</li>
            </ul>
        </div>
    );
}

export default Information;