import React from 'react';

function CompetencesLi({classes,children})
{
    return <li>
        <i className={`fab ${classes} fa-fw`}></i>
        <strong> {children}</strong>
    </li>
}

function Competences(props) {
    return (
        <section className="competences">
            <h2>Mes compétences</h2>
            <ul>
                <CompetencesLi classes="fa-html5">Html</CompetencesLi>
                <CompetencesLi classes="fa-css3">CSS</CompetencesLi>
                <CompetencesLi classes="fa-js">Javascript</CompetencesLi>
                <CompetencesLi classes="fa-fa-react">React</CompetencesLi>
                <CompetencesLi classes="fa-angular">angular</CompetencesLi>
                <CompetencesLi classes="fa-php">PHP</CompetencesLi>
                <CompetencesLi classes="fa-sympfony">Symfony</CompetencesLi>
                <CompetencesLi classes="fa-wordpress">Wordpress</CompetencesLi>
            </ul>
        </section>
    );
}

export default Competences;